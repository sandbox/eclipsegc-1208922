<?php

/**
 * @file
 * Views include file containing views hooks.
 *
 * This file is included automatically by Views, on-demand. It is registered
 * using hook_views_api() in the primary module, and the code is only loaded
 * when views hooks are needed.
 */

/**
 * Implementation of hook_views_plugins().
 */
function calendar_views_plugins() {
  return array(
    'pager' => array(
      'calendar_pager_month' => array(
        'title' => t('Month Calendar'),
        'help' => t('Page by the month against a date field.'),
        'handler' => 'calendar_pager_month_plugin_pager',
        'help topic' => 'calendar-pager-month',
        'uses options' => TRUE,
      ),
      /*'calendar_pager_year' => array(
        'title' => t('Year Calendar'),
        'help' => t('Page by the year against a date field.'),
        'handler' => 'calendar_pager_year_plugin_pager',
        'help topic' => 'calendar-pager-year',
        'uses options' => TRUE,
      ),
      'calendar_pager_week' => array(
        'title' => t('Week Calendar'),
        'help' => t('Page by the week against a date field.'),
        'handler' => 'calendar_pager_week_plugin_pager',
        'help topic' => 'calendar-pager-week',
        'uses options' => TRUE,
      ),
      'calendar_pager_day' => array(
        'title' => t('Day Calendar'),
        'help' => t('Page by the day against a date field.'),
        'handler' => 'calendar_pager_day_plugin_pager',
        'help topic' => 'calendar-pager-day',
        'uses options' => TRUE,
      ),*/
    ),
  );
}
